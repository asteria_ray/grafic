#include <stdio.h>
#include <graphics.h>
#include <math.h>
#include "gr.h"

const int X0 = 100, Y0 = 400;// ���������� ������ ���������
const float k = 15;// �������

int osx (float x)
{
    return X0+k*x;
}

int osy (float y)
{
    return Y0-k*y;
}

float my_xx ( float x, int a, int b, int c )
{
    return a*x*x + b*x + c;
}

void Axes()
{
    line ( X0, 0, X0, 599 ); // ��� ox
    line ( 0, Y0, 799, Y0 ); // ��� oy
}

void Point ( float x, float y, int color )
{
    int xe, ye;
    xe = osx(x);
    ye = osy(y);
    if ( xe >= 0 && xe < 800 && ye >= 0 && ye < 600 )
        putpixel(xe, ye, color);
}

void grafik1( my_func F, int a, int b, int a1, int b1, int c1)
{
    float x, h;
    h = 1 / k;
    for ( x = a; x <= b; x += h )
    {
        Point(x, F(x, a1, b1, c1), WHITE);
    }
}


